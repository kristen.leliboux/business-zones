const { extractZip, getDepartement, getBusinessZone, getArrondissement } = require('./tools')

function getZone(address) {
  const zip = extractZip(address)
  if (!zip) {
    const err = new Error('No zipcode found in the address')
    err.status = 400
    throw err
  }

  const departement = getDepartement(zip)
  if (!departement) {
    const err = new Error('Invalid departement')
    err.status = 400
    throw err
  }

  const arrondissement = getArrondissement(zip, departement)
  if (!arrondissement) {
    const err = new Error('Invalid zipcode or unknown arrondissement')
    err.status = 400
    throw err
  }

  const zone = getBusinessZone(arrondissement.code)

  return { zone, arrondissement }
}

module.exports = {
  getZone
}
