const turf = require('@turf/turf')
const { idf, paca } = require('../../data/departements')
const communes = require('../../data/code-postal-coordinates.json')
const arrondissements = require('../../data/arrondissements.json')

function pointInGeoShape(shape, point) {
  const turfPoints = turf.points([point])
  const turfShape = turf.geometry(shape.type, shape.coordinates)

  const lookup = turf.pointsWithinPolygon(turfPoints, turfShape)

  return !!lookup.features.length
}

function extractZip(address) {
  const found = address.match(/\b\d{5}\b/g)
  if (found && found.length) {
    return found[found.length-1]
  }
}

function getDepartement(zip) {
  const departement = zip.substring(0, 2)
  if (departement === '97' || departement === '98')
    return zip.substring(0, 3)
  return departement
}

function getBusinessZone(arrondissementCode) {
  if (idf.includes(arrondissementCode))
    return 'idf'
  if (paca.includes(arrondissementCode))
    return 'paca'
  return 'other'
}

function getArrondissement(zip, departement) {
  const commune = communes[zip]
  for(const arrondissement of arrondissements[departement]) {
    if (pointInGeoShape(arrondissement.shape, commune)) {
      const { code, name } = arrondissement
      return { code, name }
    }
  }
}

module.exports = {
  extractZip,
  getDepartement,
  getBusinessZone,
  getArrondissement,
}
