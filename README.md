# iw-server

A REST API to convert postal addresses to arrondissements and business zones.

## Configuration and installation

There's no configuration. The server is running on port 3000 in dev mode,
80 in prod mode. After a checkout just run:

```
npm install
```

## Run in dev mode

Will restart the server on code changes. Will run on port 3000.

```
npm run dev
```

Open http://localhost:3000/doc in your browser for a Swagger documentation.

## Run in prod mode

```
npm start
```

This will run the server on the port 80. A live version has been deployed here:
https://business-zones.herokuapp.com/doc

## Run unit tests

Some functions are tested. Unit test have to be completed.

```
npm test
```
