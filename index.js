const { version } = require('./package.json')
const express = require('express')
const YAML = require('yamljs')
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = YAML.load('./swagger.yaml')

const zoneService = require('./services/zone')
const app = express()
const env = process.env.ENV || 'prod'
const port = parseInt(process.env.PORT) || (env === 'prod' ? 80 : 3000)

function errorHandler(err, req, res, next) { // eslint-disable-line no-unused-vars
  res.status(err.status || 500)
  if (env === 'dev') {
    console.error(err)
  }
  res.json({ message: err.message, data: err.data })
}

app.get('/', (req, res) => {
  return res.json({
    status: 'upAndRunning',
    version,
  })
})

app.get('/zone', (req, res, next) => {
  try {
    const { address } = req.query
    if (!address || address.length > 500) {
      const err = new Error('Address missing or too long (max is 500 characters).')
      err.status = 400
      throw err
    }

    const result = zoneService.getZone(address)
    return res.json(result)
  } catch(err) {
    next(err)
  }
})

app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

app.use((req, res) => {
  return res.status(404).send({ message: 'Not found' })
})

app.use(errorHandler)

app.listen(port, function() {
  console.log(`Listening on ${port}`)
})
