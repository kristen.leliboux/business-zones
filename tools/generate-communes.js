const fs = require('fs')
const communes = require('./correspondance-code-insee-code-postal.json')

const mapping = {}

console.log('Generating a mapping between postal code and geopoints2D')

for(const commune of communes) {
  mapping[commune.fields.postal_code] = commune.geometry.coordinates
}

const str = JSON.stringify(mapping)
fs.writeFileSync('./data/code-postal-coordinates.json', str)

console.log(`Success. ${communes.length} communes processed to ./data/code-postal-coordinates.json`)
