const fs = require('fs')
const arrondissements = require('./contours-geographiques-des-arrondissements-departementaux-2019.json')

const mapping = {}

console.log('Generating a mapping between departements and a list of arrondissements with their geoshape 2D')

for(const arrondissement of arrondissements) {
  if (!mapping[arrondissement.fields.insee_dep]) {
    mapping[arrondissement.fields.insee_dep] = []
  }

  if (arrondissement.fields.geo_shape.type !== 'Polygon' && arrondissement.fields.geo_shape.type !== 'MultiPolygon') {
    console.log(arrondissement)
    throw new Error('Unknown shape')
  }

  mapping[arrondissement.fields.insee_dep].push({
    name: arrondissement.fields.nom_de_l_arrondissement,
    code: arrondissement.fields.code_arrondissement,
    shape: arrondissement.fields.geo_shape,
  })
}

const str = JSON.stringify(mapping)
fs.writeFileSync('./data/arrondissements.json', str)

console.log(`Success. ${arrondissements.length} arrondissements processed to ./data/arrondissements.json`)
