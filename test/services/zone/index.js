const expect = require('expect.js')
const { extractZip, getDepartement, getBusinessZone } = require('../../../services/zone/tools')

describe('Zone service', function() {
  describe('extractZip', function() {
    it('gives zipcode of a standard address', function() {
      const address = '48 avenue de Villiers, 75017 Paris'
      const zip = extractZip(address)
      expect(zip).to.be('75017')
    })
    it('gives undefined for a fake address', function() {
      const address = 'blah blah'
      const zip = extractZip(address)
      expect(zip).to.be(undefined)
    })
    it('selects the last 5-digit number', function() {
      const address = '44250 Long Road, 75001 Paris 1er arrondissement'
      const zip = extractZip(address)
      expect(zip).to.be('75001')
    })
    it('ignores non 5-digit numbers as long as Wikipedia is right', function() {
      const address = '2 rue du Louvre, 7501 Paris 1er arrondissement'
      const zip = extractZip(address)
      expect(zip).to.be(undefined)
    })
  })

  describe('getDepartement', function() {
    it('extracts the departement for standard zips', function() {
      const departement = getDepartement('75017')
      expect(departement).to.be('75')
    })
    it('manages special cases for overseas', function() {
      const departement = getDepartement('97400')
      expect(departement).to.be('974')
    })
  })

  describe('getArrondissement', function() {
    it('has to be tested!!!', function() {
    })
  })

  describe('getBusinessZone', function() {
    it('gives idf for 751', function() {
      const zone = getBusinessZone('751')
      expect(zone).to.be('idf')
    })
    it('gives paca for 134', function() {
      const zone = getBusinessZone('134')
      expect(zone).to.be('paca')
    })
    it('gives other for other', function() {
      const zone = getBusinessZone('974')
      expect(zone).to.be('other')
    })
  })
})
